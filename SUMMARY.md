# Summary

* [About / contact](README.md)

* [Academic career](cv.md)

* [Research interests](research.md)

* [Publications](publications.md)

* [Projects / students / misc](misc.md)

* [Science for kids](jenga.md)


