2025
---
**[Le grand recencement du monde animal et végétal](https://vmiele.gitlab.io/data/sciencesetavenir-janv2025-miele.pdf)**,
L Duthoit, based on P Bonnet, F Kirchner, <u>V Miele</u>, N Mouquet,
*Sciences et avenir (spécial IA)*

<!---
**Multilayer network analysis reveals differences in protected area connectivity across taxonomic groups**,
MC Prima, L.Oliveira-Cruz, <u>V.Miele</u>, S. Si-Moussi, P. Rouveyrol, L. Suarez, W. Thuiller, *submitted*
--->

2024
--

**Zero-shot animal behavior classification with image-text foundation models**,
G Dussert, <u>V Miele</u>, C. Van Reeth, A. Delestrade, S Dray, S Chamaille-Jammes,
*BioRxiv*

**[Being confident in confidence scores: calibration in deep learning models for camera trap image sequences](https://comptes-rendus.academie-sciences.fr/biologies/item/CRBIOL_2024__347_G1_223_0)**,
G Dussert, S Chamaille-Jammes, S Dray, <u>V Miele</u>,
*Remote Sensing for Ecology and Evolution*

**ORCHAMP: an observation network for monitoring Biodiversity and Ecosystem Functioning Across Space and Time in Mountainous**,
W Thuiller, A. Saillard, 30 authors, <u>V Miele</u>, 3 authors, J Renaud,
*Comptes Rendus Biologies de l’Académie des Sciences*

**Camera traps and deep learning enable automated large-scale density estimation of wildlife in temperate forest ecosystems**,
M Henrich, C Fiderer, A Klamm, A Schneider, A Ballmann, J Stein, R Kratzer, R Reiner, S Greiner, S Twietmeyer, T Rönitz, V Spicher, S Chamaillé-Jammes, <u>V Miele</u>, G Dussert, M Heurich,
*Submitted*

2023
--

**The DeepFaune initiative: a collaborative effort towards the automatic identification of the European fauna in camera-trap images**,
N Rigoudy, G Dussert, the DeepFaune consortium, B Spataro, <u>V Miele</u>, S Chamaillé Jammes,
*European Journal of Wildlife Research*

**DeepFaune : vers un traitement automatisé des images de pièges photographiques**,
S Chamaillé-Jammes, <u>V Miele</u>, N Rigoudy, G Dussert, B Spataro,
*Biodiversité, des clés pour agir 4*

**Nine tips for ecologists using machine learning**,
M Desprez, *V Miele*, O Gimenez,
*arXiv:2305.10472*

**Quantifying the overall effect of biotic interactions on species communities along environmental gradients**,
M Ohlmann, G Poggiato, S Dray, W Thuiller, C Matias, <u>V Miele</u>,
*Ecological Modelling*

**Mobbing calls of seven species of Parids under the paradigm of the FME-D combination**,
A Salis, T Lengagne, <u>V Miele</u>, K Sieving, H Henry, JP Léna, *ResearchSquare*

2022
--

**Using latent block models to detect structure in ecological networks**,
J Aubert, P Barbillon, S Donnet, <u>V Miele</u>,
*ISTE Editions*

2021
--

**An appraisal of graph embeddings for comparing trophic network architectures**,
C Botella, S Dray, C Matias, <u>V Miele</u>, W Thuiller,
*Methods in Ecology and Evolution*

**Revisiting animal photo‐identification using deep metric learning and network analysis**,
V Miele, G Dussert, B Spataro, S Chamaillé-Jammes, D Allainé, C. Bonenfant,
*Methods in Ecology and Evolution*

**Images, écologie et deep learning**,
<u>V Miele</u>, S Dray, O Gimenez,
*Regards sur la biodiversité - Société Française d’Écologie et d’Évolution*

2020
--

**Core–periphery dynamics in a plant–pollinator network**
<u>V Miele</u>, R Ramos‐Jiliberto, DP Vázquez,
*Journal of Animal Ecology*

**Core-periphery structure in mutualistic networks: an epitaph for nestedness?**,
AM Martín González, DP Vázquez, R Ramos-Jiliberto, SH Lee, <u>V Miele</u>,
*bioRxiv, 2020.04. 02.021691*

**Deep learning for species identification of modern and fossil rodent molars**,
<u>V Miele</u>, G Dussert, T Cucchi, S Renaud,
*bioRxiv*

**Dynamic of social interactions: analysis and modeling from movement in several large mammal species**,
T Genissel, <u>V Miele</u>, A Loison,
*HAL*


2019
--

**Nine quick tips for analyzing network data**,
<u>V Miele</u>, C Matias, S Robin, S Dray
*PLOS Computational Biology 15 (12), e1007434*


**Non-trophic interactions strengthen the diversity—functioning relationship in an ecological bioenergetic network model**,
<u>V Miele</u>, C Guill, R Ramos-Jiliberto, S Kéfi
*PLoS Computational Biology 15 (8), e1007269*

**Global survey of mobile DNA horizontal transfer in arthropods reveals Lepidoptera as a prime hotspot**,
D Reiss, G Mialdea, <u>V Miele</u>, DM de Vienne, J Peccoud, C Gilbert, L Duret, ...
*PLoS genetics 15 (2), e1007965*

**Diversity indices for ecological networks: a unifying framework using Hill numbers**,
M Ohlmann, <u>V Miele</u>, S Dray, L Chalmandrier, L O’Connor, W Thuiller,
*Ecology Letters*

2018
--

**Playing hide and seek with repeats in local and global de novo transcriptome assembly of short RNA-seq reads**,
L Lima, B Sinaimeri, G Sacomoto, H Lopez-Maestre, C Marchet, <u>V Miele</u>...,
*Algorithms for molecular biology 12 (1), 1-19*

2017
--

**Revealing the hidden structure of dynamic ecological networks**,
<u>V Miele</u>, C Matias
*Royal Society open science 4 (6), 170251*

**Inferring the timing of territoriality and rut in male roe deer from movements? Some preliminary results-and new perspectives**, 
N Morellet, <u>V Miele</u>, C Bonenfant, 
*EURODEER meeting*

**Ecological networks to unravel the routes to horizontal transposon transfers**,
S Venner, <u>V Miele</u>, C Terzian, C Biémont, V Daubin, C Feschotte, ...
*PLoS biology 15 (2), e2001536*

**Réseaux et connectivité**
EE Cossart, C Fontaine, G Marchand, M Balasse, S Bréhard, C Manen, ...<u>V Miele</u>,...
*Prospectives de l’Institut Écologie et environnement du CNRS, 187-193*

**Statistical clustering of temporal networks through a dynamic stochastic block model**,
C Matias, <u>V Miele</u>
*Journal of the Royal Statistical Society: Series B (Statistical Methodology)*


2016
--

**Colib’read on galaxy: a tools suite dedicated to biological information extraction from raw NGS reads**,
Y Le Bras, O Collin, C Monjeaud, V Lacroix, É Rivals, C Lemaitre, <u>V Miele</u>, ...
*GigaScience 5 (1), s13742-015-0105-2*

**How structured is the entangled bank? The surprisingly simple organization of multiplex ecological networks leads to increased persistence and resilience**,
S Kéfi, <u>V Miele</u>, EA Wieters, SA Navarrete, EL Berlow,
*PLoS biology 14 (8), e1002527*


**Fruiting strategies of perennial plants: a resource budget model to couple mast seeding to pollination efficiency and resource allocation strategies**,
S Venner, A Siberchicot, PF Pélisson, E Schermer, MC Bel-Venner, ...<u>V Miele</u>...,
*The American Naturalist 188 (1), 66-75*

**Calcul parallèle avec R**,
<u>V Miele</u>, V Louvet,
*EDP Sciences*

2015
--

**DNA physical properties and nucleosome positions are major determinants of HIV-1 integrase selectivity**,
M Naughtin, Z Haftek-Terreau, J Xavier, S Meyer, M Silvain, ...<u>V Miele</u>...,
*PloS one 10 (6), e0129427*

2014
--

**Navigating in a sea of repeats in rna-seq without drowning**,
G Sacomoto, B Sinaimeri, C Marchet, <u>V Miele</u>, MF Sagot, V Lacroix,
*International Workshop on Algorithms in Bioinformatics, 82-96*

**Spatially constrained clustering of ecological networks**,
<u>V Miele</u>, F Picard, S Dray,
*Methods in Ecology and Evolution 5 (8), 771-779*

**Fast and parallel algorithm for population-based segmentation of copy-number profiles**,
G Rigaill, <u>V Miele</u>, F Picard,
*Computational Intelligence Methods for Bioinformatics and Biostatistics*

2013
--

**New developments in KisSplice: Combining local and global transcriptome assemblers to decipher splicing in RNA-seq data**,
A Julien-Laferriere, G Sacomoto, R Chikhi, E Scaon, D Parsons, MF Sagot, ...<u>V Miele</u>...,
*Journées Ouvertes en Biologie, Informatique et Mathématiques (JOBIM)*

2012
--

**High-quality sequence clustering guided by network topology and multiple alignment likelihood**,
<u>V Miele</u>, S Penel, V Daubin, F Picard, D Kahn, L Duret,
*Bioinformatics 28 (8), 1078-1085*

2011
--

**Ultra-fast sequence clustering from similarity networks with SiLiX**,
<u>V Miele</u>, S Penel, L Duret,
*BMC bioinformatics 12, 1-9*

2010
--

**Strategies for online inference of model-based clustering in large and growing networks**,
H Zanghi, F Picard, <u>V Miele</u>, C Ambroise,
*The Annals of Applied Statistics*

2009
--

**Deciphering the connectivity structure of biological networks using MixNet**,
F Picard, <u>V Miele</u>, JJ Daudin, L Cottret, S Robin,
*BMC bioinformatics 10 (6), 1-11*

2008
--

**Fast online graph clustering via Erdős–Rényi mixture**,
H Zanghi, C Ambroise, <u>V Miele</u>,
*Pattern recognition 41 (12), 3592-3599*

**DNA physical properties determine nucleosome occupancy from yeast to fly**,
<u>V Miele</u>, C Vaillant, Y d'Aubenton-Carafa, C Thermes, T Grange,
*Nucleic acids research 36 (11), 3746-3756*


2006
--

**A reversible jump Markov chain Monte Carlo algorithm for bacterial promoter motifs discovery**,
P Nicolas, AS Tocquet, <u>V Miele</u>, F Muri,
*Journal of Computational Biology 13 (3), 651-667*

2005
--

**seq++: analyzing biological sequences with a range of Markov-related models**,
<u>V Miele</u>, PY Bourguignon, D Robelin, G Nuel, H Richard,
*Bioinformatics 21 (11), 2783-2784*
