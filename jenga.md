------

### KIDS, HOW "AI" LEARNS TO RECOGNIZE ANIMALS IN IMAGES?

My PhD student Gaspard Dussert developed a video game called [Cosmic Camera Bot](https://xrnz.itch.io/cosmicbot). The goal: understanding how an IA can be trained to recognize species in camera trap images. Follow Dr. Muonic who is in charge of monitoring the biodiversity of the Alphagrax planet: he has set up camera traps all over the planet to photograph wild species!!


<p style="text-align: center;">
<a href="https://xrnz.itch.io/cosmicbot" target="_blank">
 <img src="img/cosmicbot.jpg" alt="Cosmic bot" width="240" height="180" border="10" /></a><br>
 <i> The `cosmic bot` video game (click on image to open the video game)</i>
</p>

------

### KIDS, WHAT DO YOU KNOW ABOUT ECOLOGICAL INTERACTIONS?

With my CNRS colleague Sonia Kéfi, we aim at transmitting our knowledge on the conecpt of "interaction" to school children :


<p style="text-align: center;">
<img width="30%" src="img/hetraiesapiniere.jpg" alt="Hetraie sapinière"><br>
<i>Our interactive game about interactions in a "Hétraie-sapinière" in Chartreuse<br> (copyright CNRS - reuse and reproduction forbidden) </i>
</p>


<p style="text-align: center;">
<img width="30%" src="img/ejj_dessin.jpg" alt="Drawing"><br>
<i>A drawing after our talk @ école Jean Jaurès in Chambéry<br> (copyright CNRS - reuse and reproduction forbidden) </i>
</p>

<p style="text-align: center;">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=QLXWh7zkRmg" target="_blank">
 <img src="https://img.youtube.com/vi/QLXWh7zkRmg/0.jpg" alt="Biodiversité : tous différents mais tous reliés !" width="240" height="180" border="10" /></a><br>
 <i> Our video "Biodiversity: all different but all linked to each other!" (click on image to open the video)</i>
</p>

