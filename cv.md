Ingénieur de Recherche Hors Classe CNRS

------

- 2024- Scientific advisor for AI at [CNRS Ecology & Environment](https://www.inee.cnrs.fr)
- 2024- Member of the scientific board of [MIAI](https://miai.univ-grenoble-alpes.fr) 
- 2022- Member of the scientific board of [CNRS AISSAI center](https://aissai.cnrs.fr/)
- 2017- Member of the leading board of [GdR EcoStat](https://sites.google.com/site/gdrecostat/)
- 2019-2023 Member of the scientific board of [IXXI Complex Systems Institute](http://www.ixxi.fr/)
- 2009-2017 Member of the leading board of [CNRS Calcul group](https://calcul.math.cnrs.fr/)

------

- 2024- Member of [LECA - Alpine Ecology lab](https://leca.osug.fr/) (permanent visitor since 2018)
- 2008-2023 Member of [Biometry & Evolutionary Biology lab](https://lbbe.univ-lyon1.fr)
- 2001-2007 Member of [Statistics & Genome lab](https://www.genopole.fr/)
