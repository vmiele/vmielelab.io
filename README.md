<meta name="google-site-verification" content="hd1OfwYSORbJsPXZIoJozCiUzJx4o6FOgml2kc6dUhQ" />

<h1 style="text-align: center;">Vincent Miele - CNRS</h1>
<h3 style="text-align: center;">Machine learning for community ecology</h3>

With a background in mathematics and computer science, I bring my machine learning skills to community ecology projects. I am working at CNRS (*ingénieur de recherche hors classe*) in the Alpine Ecology Lab @ Chambéry/Grenoble.

<p style="text-align: center;">
<img width="50%" src="img/vincentmiele.jpg" alt="Vincent Miele CNRS"> 
</p>


<p style="text-align: center;">
<a href="https://en.wikipedia.org/wiki/Postcard">
vincent.miele@cnrs.fr
</a>
</p>



<p style="text-align: center;">
Batiment Belledonne 8A<br>
Campus Technolac<br>
<a href="https://www.skaping.com/grandlac/bourgetdulac">
Le Bourget du lac
</a>
<br>
Savoie - France
</p>
