------

## ECOLOGICAL NETWORKS

Fascinated since my studies by the concept of _graph_, I contribute to Network Science, in particular by analyzing or modeling ecological networks with original methods. I consider that the notion of interaction between species, the key concept for any ecological network, is fundamental for understanding the functioning (and the future) of ecological communities. I advocate the consideration of all types of interaction, from facilitation to competition, from mutualism to trophic relationships, etc.

<p style="text-align: center;">
<img width="30%" src="img/metanetwork.jpg" alt="Network Science"><br>
<i>An ecological network, represented by our late friend Marc Ohlmann.</i>
</p>

------

## COMPUTER VISION FOR ECOLOGISTS

I was absorbed by the 2010s revolution around deep learning for computer vision, because of the usefulness of these techniques for ecologists. I develop vision models adapted to images from sensors, in particular camera traps, and I contribute to monitoring and dissemination of recent developments in deep learning.

<p style="text-align: center;">
<img width="30%" src="img/deepfaune.jpg" alt="DeepFaune"><br>
<i>Our DeepFaune app</i>
<a href="https://www.deepfaune.cnrs.fr">(https://www.deepfaune.cnrs.fr)</a>
</p>

------

## MACHINE LEARNING in its broad sense

My first day at CNRS was dedicated to understanding Hidden Markov Models, a standard of machine learning (we said “statistics" at the time...). Since then, I have used supervised or unsupervised learning techniques (and other variants) as an expert. I consider that doing good machine learning is more complicated than it seems, it means working well on the data level, and thinking carefully about the design of the training phase. Lots of recipes to follow...
